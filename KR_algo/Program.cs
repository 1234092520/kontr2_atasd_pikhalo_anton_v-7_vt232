﻿using System;
using System.Collections.Generic;
using System.Linq;

class Node
{
    public char? Symbol { get; set; }
    public double Frequency { get; set; }
    public Node Left { get; set; }
    public Node Right { get; set; }
}

class Program
{
    static Random random = new Random();

    static Node BuildTree(List<char> symbols, List<double> frequencies)
    {
        List<Node> nodes = symbols.Select((symbol, index) => new Node { Symbol = symbol, Frequency = frequencies[index] }).ToList();
        while (nodes.Count > 1)
        {
            nodes = nodes.OrderBy(node => node.Frequency).ToList();
            Node left = nodes[0];
            Node right = nodes[1];
            Node parent = new Node { Frequency = left.Frequency + right.Frequency };
            parent.Left = left;
            parent.Right = right;
            nodes = nodes.Skip(2).ToList();
            nodes.Add(parent);
        }
        return nodes[0];
    }

    static Dictionary<char, string> TraverseTree(Node root, string code = "", Dictionary<char, string> mapping = null)
    {
        if (mapping == null)
            mapping = new Dictionary<char, string>();
        if (root != null)
        {
            if (root.Symbol != null)
                mapping[(char)root.Symbol] = code;
            mapping = TraverseTree(root.Left, code + "0", mapping);
            mapping = TraverseTree(root.Right, code + "1", mapping);
        }
        return mapping;
    }

    static Tuple<string, Dictionary<char, string>> ShannonFanoCompress(string text)
    {
        List<char> symbols = text.Distinct().ToList();
        List<double> frequencies = symbols.Select(symbol => (double)text.Count(ch => ch == symbol) / text.Length).ToList();

        Node root = BuildTree(symbols, frequencies);
        Dictionary<char, string> codes = TraverseTree(root);

        string compressedText = string.Join("", text.Select(ch => codes[ch]));

        return Tuple.Create(compressedText, codes);
    }

    static string ShannonFanoDecompress(string compressedText, Dictionary<char, string> codes)
    {
        Dictionary<string, char> reversedCodes = codes.ToDictionary(kv => kv.Value, kv => kv.Key);
        string decodedText = "";
        string code = "";
        foreach (char bit in compressedText)
        {
            code += bit;
            if (reversedCodes.ContainsKey(code))
            {
                decodedText += reversedCodes[code];
                code = "";
            }
        }
        return decodedText;
    }

    static void Main(string[] args)
    {
        Console.WriteLine("Введіть текст для стисення:");
        string text = Console.ReadLine();

        var compressedTextAndCodes = ShannonFanoCompress(text);
        string compressedText = compressedTextAndCodes.Item1;
        var codes = compressedTextAndCodes.Item2;
        Console.WriteLine("Стиснутий текст: " + compressedText);
        Console.WriteLine("Codes:");
        foreach (var kvp in codes)
        {
            Console.WriteLine(kvp.Key + ": " + kvp.Value);
        }
        string decompressedText = ShannonFanoDecompress(compressedText, codes);
        Console.WriteLine("Розпакований текст: " + decompressedText);
    }
}
